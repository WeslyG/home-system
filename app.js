import bodyParser from 'body-parser';
import express from 'express';
import * as mongoose from 'mongoose';
import {
  getUsers,
  createUser,
  validateUser,
  validateUserById,
  deleteUser,
  updatePassword
} from './src/controllers/userControllers';
import { 
  getRoles,
  createRoles,
  deleteRoles,
  validateRoles,
  validateRolesById
} from './src/controllers/rolesControllers';

const app = express();
app.use(bodyParser.json())

mongoose.connect('mongodb://localhost:27017/test', {
  useNewUrlParser: true,
  versionKey: false
});

// users route
app.route('/users')
  .get(async(req, res) => {
    res.status(200).send(await getRoles());
  })
  .put(async(req, res) => {
    if (!req.body.name || !req.body.password) {
      res.status(400).send({ message: "name and password require!"});
    }
    const valid = await validateUser(req.body.name);
    if (valid === true) {
      res.status(201).send(await createUser(req.body.name, req.body.password));
    } else {
      res.status(400).send({ message: "user exist"});
    }
  })
  .delete(async(req, res) => {
    if (!req.body.id) {
      res.status(400).send({ message: "id is require"});
    }
    const changedUser = await deleteUser(req.body.id);
    res.status(200).send(changedUser);
  })
  .post(async(req, res) => {
    if (!req.body.id || !req.body.password || !req.body.newPassword) {
      res.status(400).send({ message: "id, password and newPassword is require"});
    }
    const valid = await validateUserById(req.body.id);
    if (valid === true) {
      res.status(200).send(await updatePassword(req.body.id, req.body.password, req.body.newPassword))
    } else {
      res.status(400).send({ message: "user id not found"});
    }
  })


// /roles
app.route('/roles')
  .get(async (req, res) => {
    res.status(200).send(await getRoles());
  })
  .put(async (req, res) => {
    if (!req.body.name) {
      res.status(400).send({ message: "name is require"});
    } else {
      const valid = await validateRoles(req.body.name);
      if (valid === true) {
        res.status(200).send(await createRoles(req.body.name));
      } else {
        res.status(400).send({ message: "roles exist"});
      };
    }
  })
  .delete(async(req, res) => {
    if (!req.body.id) {
      res.status(400).send({message: "id is require"});
    } else {
      const valid = validateRolesById(req.body.id);
      if (valid === true) {
        res.status(200).send(await deleteRoles(req.body.id));
      } else {
        res.status(400).send({ message: "roles not found"})
      }
    }
  })

// started
app.listen(3000, () => {
  console.log('I am started on port 3000');
});


    // const user = new Users({
    //   name: 'Любовь',
    //   password: '123',
    //   deleted: false
    // });
    // user.save().then(() => res.send({
    //   message: user
    // }));


  // const user = [{
  //   name: "Люба",
  //   password: "123",
  //   deleted: false
  // },
  // {
  //   name: "Надя",
  //   password: "2222",
  //   deleted: false
  // }]


//  Promices + Async test
// const testFunc = async () => {
//   return new Promise((res, rej) => {
//       res('test');
//   });
// }

// const main = async () => {
//   console.log(await testFunc());
// }

// main();