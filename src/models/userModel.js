import mongoose from 'mongoose';

export default mongoose.model('Users', {
  name: String,
  password: String,
  deleted: Boolean
});