import mongoose from 'mongoose';

export default mongoose.model('Roles', {
  name: String,
  // description: String,
  deleted: Boolean
});