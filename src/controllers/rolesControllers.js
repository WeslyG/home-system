import Roles from '../models/rolesModel';

export const getRoles = async () => {
  return Roles.find({
    deleted: false
  });
}

export const validateRoles = async name => {
    const roles = await Roles.find({ name });
    if (roles.length === 0) {
      return true;
    } else {
      return false;
    }
}

export const validateRolesById = async id => {
  const roles = await Roles.find({
    _id: id
  });
  if (roles.length === 0) {
    return false;
  } else {
    return true;
  }
}

export const createRoles = async name => {
    const roles = new Roles ({
      name,
      deleted: false
    });
    return roles.save();
}