import Users from '../models/userModel';

export const getUsers = async () => {
  return Users.find({ deleted: false });
}

export const createUser = async (name, password) => {
  const user = new Users({
    name,
    password,
    deleted: false
  });
  return user.save();
};

export const validateUser = async name => {
  const exist = await Users.find({ name });
  if (exist.length === 0) {
    return true;
  } else {
    return false;
  }
};

export const validateUserById = async id => {
  const exist = await Users.find({_id: id});
  if (exist.length === 0) {
    return false;
  } else {
    return true;
  }
};

export const deleteUser = async id => {
  return Users.findOneAndUpdate(id, {deleted: true});
}

export const updatePassword = async (id, password, newPassword) => {
  const user = await Users.find({ _id: id, password});
  if (user.length === 0) {
    return { message: "bad current password"}
  } else {
    await Users.findOneAndUpdate({_id: id}, { password: newPassword });
    return Users.findOne({_id: id});
  }
}